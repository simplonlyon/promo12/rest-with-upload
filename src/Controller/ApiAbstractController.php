<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;




abstract class ApiAbstractController extends AbstractController
{
    /**
     * L'entité pour laquelle on veut créer un contrôleur rest
     */
    protected $entity;
    /**
     * Le formulaire à associer à ce contrôleur rest
     */
    protected $type;
    public function __construct(string $entity, string $type)
    {
        $this->entity = $entity;
        $this->type = $type;
    }

    /**
     * Pour utiliser la pagination : http://localhost:8000/api/exemple?limit=5&page=2
     * @Route(methods="GET")
     */
    public function getAll(Request $request)
    {
        $limit = 10;
        $page = 1;
        if($request->get('limit')) {
            $limit = $request->get('limit');
        }
        if($request->get('page')) {
            $page = $request->get('page');
        }
        $repo = $this->getDoctrine()->getRepository($this->entity);
        return $this->json($repo->findBy([], [], $limit, ($page-1)*$limit));
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function getOne(int $id)
    {
        $repo = $this->getDoctrine()->getRepository($this->entity);
        $entity = $repo->find($id);
        if ($entity) {
            return $this->json($entity);
        }
        return $this->json(null, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route(methods="POST")
     * @Route("/{id}", methods="PATCH")
     */
    public function add(Request $request, EntityManagerInterface $manager, int $id = null)
    {

        $entity = null;
        if ($id == null) {
            $entity = new $this->entity();
        } else {
            $repo = $this->getDoctrine()->getRepository($this->entity);
            $entity = $repo->find($id);
            if (!$entity) {
                return $this->json(null, Response::HTTP_NOT_FOUND);
            }
        }
        $form = $this->createForm($this->type, $entity, [
            'csrf_protection' => false
        ]);

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $this->prePersist($entity);
            $manager->persist($entity);
            $manager->flush();
            return $this->json($entity, Response::HTTP_CREATED);
        }

        return $this->json($form->getErrors(true), Response::HTTP_BAD_REQUEST);
    }
    /**
     * @Route("/{id}",methods="DELETE")
     */
    public function delete(int $id, EntityManagerInterface $manager)
    {
        $repo = $this->getDoctrine()->getRepository($this->entity);
        $entity = $repo->find($id);
        if (!$entity) {
            return $this->json(null, Response::HTTP_NOT_FOUND);
        }
        $manager->remove($entity);
        $manager->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    protected function prePersist($entity) {}
}
