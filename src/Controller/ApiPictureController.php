<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\PictureType;
use App\Repository\PictureRepository;
use App\Service\UploadService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/api/picture", name="api_picture")
     */
class ApiPictureController extends ApiAbstractController
{
    private $uploader;
    public function __construct(UploadService $uploader) {
        parent::__construct(Picture::class, PictureType::class);
        $this->uploader = $uploader;
    }

    protected function prePersist($entity)
    {
        $filepath = $this->uploader->uploadBase64($entity->imageFile);
        $entity->setImagePath($filepath);
        $entity->imageFile = '';
    }
}
