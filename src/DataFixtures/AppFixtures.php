<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $picture = new Picture();
        $picture->setTitle('Look at this photograph')
        ->setDescription('Everytime I do it makes me laugh')
        ->setImagePath('fixture.jpg');

        $manager->persist($picture);
        $manager->flush();
    }
}
