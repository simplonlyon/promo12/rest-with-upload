<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;


class UploadService {

    
    public function upload(File $file, string $absolutePath = ""):string {
        
        $path = __DIR__."/../../public/uploads";

        //On vérifie si le dossier existe, si non, on le crée
        if(!is_dir($path)) {
            mkdir($path);
        }
        //On génère un nom de fichier unique avec l'extension qu'il faut
        $filename = uniqid() . "." . $file->guessExtension();
        //On met le fichier dans le dossier qu'on a spécifié au dessus
        $file->move($path, $filename);
        //On return le chemin éventuellement en absolu vers le fichier
        return $absolutePath . "/uploads/" . $filename;
    }

    public function uploadBase64(string $base64, string $absolutePath = "") {
        $path = __DIR__."/../../public/uploads";

        $imgdata = base64_decode($base64);

        $f = finfo_open();

        $mime_type = finfo_buffer($f, $imgdata, \FILEINFO_MIME_TYPE);
        $split = explode( '/', $mime_type );
        $type = $split[1];
        $filename = $path . '/' . uniqid() . '.' . $type;
        $file = fopen($filename, 'wb');
        fwrite($file, $imgdata);
        fclose($file);
        return $absolutePath . '/uploads/' . $filename;
    }
}
