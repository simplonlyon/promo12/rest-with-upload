## How To Use
1. Cloner le projet
2. Mettre le DATABASE_URL dans le .env.local
3. Créer la bdd (`bin/console do:da:cr`)
4. Exécuter les migrations (`bin/console do:mi:mi`)
5. Exécuter les fixtures (`bin/console do:fi:lo`)
6. Lancer le serveur (`symfony server:start`)
7. CRUD dispo en REST sur l'url http://localhost:8000/api/picture


## Exercice Nativescript
Faire une application nativescript angular qui permettra de lister les 
pictures de cette API ainsi que de rajouter une nouvelle picture via 
l'appareil photo du téléphone

Pour ce faire, utiliser ce plugin nativescript permettant l'accès à l'appareil photo du device : https://github.com/NativeScript/nativescript-camera